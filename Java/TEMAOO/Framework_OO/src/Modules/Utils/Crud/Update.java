package Modules.Utils.Crud;

import Modules.Classes.football;
import Modules.Classes.running;
import Modules.Classes.swimming;
import Modules.Classes.tennis;
import Modules.Utils.FunctionsDate;
import utils.ButtonBox;
import utils.ComboBox;
import utils.Validates_Data;

public class Update {

//////////////////////////////////////////////////////

	public static swimming update_swimming(swimming object) { // MODIFICAR SWIMMING
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String size[] = { " XS", " S ", " M ", " L " };
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" SWIMCUIR COLOR ", " SWIMCUIT SIZE " ," DATE OF DELIVERY " };

		switch (ButtonBox.buttonsOptionsExit("Option:", "Option", options)) {
		case 0:
			object.setUser_type(ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users));
			break;
		case 1: //
			object.setCity_shop(Validates_Data.pidecity());
			break;
		case 2: //
			object.setHabitual_client(
					ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients));
			break;
		case 3:
			object.setdni(Validates_Data.pidedni());
			break;
		case 4:
			object.setdate_nac(FunctionsDate.date_nac("INTRO YOUR DATE OF BITH"));
			break;
		case 5: //
			object.setSwimcuit_color(Validates_Data.swimciut_color());
			break;
		case 6: //
			object.setSwimcuit_size(ComboBox.comboBoxV(" Choose the size : ", " SIZE", size));
			break;
		case 7:
			object.setdateAfter(FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY"));
			break;
		}

		return object;

	}

	//////////////////////////////////////////////////////

	public static football update_football(football object) { // MODIFICAR FOOTBALL
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String brand_boats[] = { "NIKE ", " ADIDAS ", " PUMA " };
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" FOOTBALL TYPE ", " BRAND ", " SIZE " ," DATE OF DELIVERY " };

		switch (ButtonBox.buttonsOptionsExit("Option :", "Option", options)) {
		case 0:
			object.setUser_type(ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users));
			break;
		case 1: //
			object.setCity_shop(Validates_Data.pidecity());
			break;
		case 2: //
			object.setHabitual_client(
					ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients));
			break;
		case 3:
			object.setdni(Validates_Data.pidedni());
			break;
		case 4:
			object.setdate_nac(FunctionsDate.date_nac("INTRO YOUR DATE OF BITH"));
			break;
		case 5: //
			object.setFootball_type(Validates_Data.football_type());
			break;
		case 6: //
			object.setBrand(ComboBox.comboBoxV(" WHICH BRAND DO YOU WANT ? ", " BRAND ", brand_boats));
			break;
		case 7: //
			object.setSize(Validates_Data.size());
		case 8:
			object.setdateAfter(FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY"));
			break;
		}

		return object;

	}

//////////////////////////////////////////////////////

	public static running update_running(running object) { //// MODIFICAR RUNNING
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String tshirt_options[] = { " long sleeve ", " short sleeve ", "THERMAL" };
		String shorts_options[] = { "long pants ", " short pants " };
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ", " T-SHIRT  ",
				" SHORT " ," DATE OF DELIVERY " };

		switch (ButtonBox.buttonsOptionsExit("Option:", "Option", options)) {
		case 0:
			object.setUser_type(ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users));
			break;
		case 1: //
			object.setCity_shop(Validates_Data.pidecity());
			break;
		case 2: //
			object.setHabitual_client(
					ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients));
			break;
		case 3:
			object.setdni(Validates_Data.pidedni());
			break;
		case 4:
			object.setdate_nac(FunctionsDate.date_nac("INTRO YOUR DATE OF BITH"));
			break;
		case 5: //
			object.setTshirt(ComboBox.comboBoxV("CHOOSE YOUR T-SHIRT TYPE ", " T-SHIRT", tshirt_options));
			break;
		case 6: //
			object.setShorts(ComboBox.comboBoxV("CHOOSE YOUR SHORTS TYPE ", " SHORTS ", shorts_options));
		case 7:
			object.setdateAfter(FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY"));
			break;
		}

		return object;

	}

	//////////////////////////////////////////////////////

	public static tennis update_tennis(tennis object) { // MODIFICAR TENNIS
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String c_accessories[] = { "Tennis Racquets ", " Wristbands ", " Tennis Balls" };
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" ACCESSORIE " ," DATE OF DELIVERY " };

		switch (ButtonBox.buttonsOptionsExit("Option:", "Option", options)) {
		case 0:
			object.setUser_type(ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users));
			break;
		case 1: //
			object.setCity_shop(Validates_Data.pidecity());
			break;
		case 2: //
			object.setHabitual_client(
					ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients));
			break;
		case 3:
			object.setdni(Validates_Data.pidedni());
			break;
		case 4:
			object.setdate_nac(FunctionsDate.date_nac("INTRO YOUR DATE OF BITH"));
			break;
		case 5: //
			object.setAccessories(ComboBox.comboBoxV(" CHOOSE AN  ACCESSORIE :", " ACCESSORIE ", c_accessories));
		case 6:
			object.setdateAfter(FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY"));
			break;
		}

		return object;

	}

}
