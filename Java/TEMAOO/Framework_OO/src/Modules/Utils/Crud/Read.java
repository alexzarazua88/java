package Modules.Utils.Crud;

import Modules.Classes.football;
import Modules.Classes.running;
import Modules.Classes.swimming;
import Modules.Classes.tennis;
import utils.ComboBox;

public class Read {

	/////////////////////////////////////////////////////

	public static String read_swimming(swimming object) { // MOSTRAR SWIMMING
		String read = "";
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" SWIMCUIR COLOR ", " SWIMCUIT SIZE ", " DATE PURCHASE ", " DELIVERY ", "PRICE", "All" };

		switch (ComboBox.comboBoxV("Option:", "Option", options)) {
		case " USER TYPE ":
			read = ("USER_TYPE: " + object.getUser_type());
			break;
		case " CITY SHOP": //
			read = ("CITY SHOP : " + object.getCity_shop());
			break;
		case " HABITUAL CLIENT ": //
			read = ("HABITUAL CLIENT: " + object.getHabitual_client());
			break;
		case " DNI ": //
			read = ("DNI : " + object.getdni());
			break;
		case "DATE OF BIRHT  ":
			read = ("DATE OF BIRHT : " + object.getdate_nac());
			break;
		case " SWIMCUIR COLOR ": //
			read = ("SWIMCUIR COLOR: " + object.getSwimcuit_color());
			break;
		case " SWIMCUIT SIZE ": //
			read = ("SWIMCUIT SIZE: " + object.getSwimcuit_size());
			break;
		case " DATE PURCHASE ": //
			read = ("DATE PURCHASE  : " + object.getdate_buy());
			break;
		case " DELIVERY ":
			read = ("DELIVERY : " + object.delivery());
			break;
		case " PRICE ":
			read = ("PRICE :  " + object.getprice());
			break;
		case "All": // ALL
			read = object.toString();
			break;
		}

		return read;

	}

	/////////////////////////////////////////////////////

	public static String read_football(football object) { // MOSTRAR FOTBALL
		String read = "";
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" FOOTBALL TYPE ", " BRAND ", " SIZE ", " DATE PURCHASE ", " DATE OF DELIVERY ", "PRICE", "All" };

		switch (ComboBox.comboBoxV("Option:", "Option", options)) {
		case " USER TYPE ":
			read = ("USER_TYPE: " + object.getUser_type());
			break;
		case " CITY SHOP": //
			read = ("CITY SHOP : " + object.getCity_shop());
			break;
		case " HABITUAL CLIENT ": //
			read = ("HABITUAL CLIENT: " + object.getHabitual_client());
			break;
		case " DNI ":
			read = ("DNI : " + object.getdni());
			break;
		case "DATE OF BIRHT  ":
			read = ("DATE OF BIRHT : " + object.getdate_nac());
			break;
		case " FOOTBALL TYPE ": //
			read = ("FOOTBALL TYPE: " + object.getFootball_type());
			break;
		case " BRAND ": //
			read = (" BRAND: " + object.getBrand());
			break;
		case " SIZE ": //
			read = (" SIZE: " + object.getSize());
			break;
		case " DATE PURCHASE ": //
			read = ("DATE PURCHASE  : " + object.getdate_buy());
			break;
		case " DATE OF DELIVERY ":
			read = ("DELIVERY : " + object.delivery());
			break;
		case "PRICE":
			read = ("PRICE :  " + object.getprice());
		case "All": // ALL
			read = object.toString();
			break;
		}

		return read;

	}

	/////////////////////////////////////////////////////

	public static String read_running(running object) { // MOSTRAR RUNNING
		String read = "";

		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ", " T-SHIRT  ",
				" SHORT ", " DATE PURCHASE ", " DATE OF DELIVERY ", " PRICE ", "All" };

		switch (ComboBox.comboBoxV("Option:", "Option", options)) {
		case " USER TYPE ":
			read = ("USER_TYPE: " + object.getUser_type());
			break;
		case " CITY SHOP": //
			read = ("CITY SHOP : " + object.getCity_shop());
			break;
		case " HABITUAL CLIENT ": //
			read = ("HABITUAL CLIENT: " + object.getHabitual_client());
			break;
		case " DNI ":
			read = ("DNI : " + object.getdni());
			break;
		case "DATE OF BIRHT  ":
			read = ("DATE OF BIRHT : " + object.getdate_nac());
			break;
		case " T-SHIRT  ": //
			read = ("T-SHIRT: " + object.getTshirt());
			break;
		case " SHORT ": //
			read = (" SHORT: " + object.getShorts());
			break;
		case " DATE PURCHASE ": //
			read = ("DATE PURCHASE  : " + object.getdate_buy());
			break;
		case " DATE OF DELIVERY ":
			read = ("DELIVERY : " + object.delivery());
			break;
		case " PRICE ":
			read = ("PRICE :  " + object.getprice());
			break;
		case "All": // ALL
			read = object.toString();
			break;
		}

		return read;

	}

	/////////////////////////////////////////////////////

	public static String read_tennis(tennis object) { // MOSTRAR TENNIS
		String read = "";
		String[] options = { " USER TYPE ", " CITY SHOP", " HABITUAL CLIENT ", " DNI ", "DATE OF BIRHT  ",
				" ACCESSORIE ", " DATE PURCHASE ", " DATE OF DELIVERY ", " PRICE ", "All" };

		switch (ComboBox.comboBoxV("Option:", "Option", options)) {
		case " USER TYPE ":
			read = ("USER_TYPE: " + object.getUser_type());
			break;
		case " CITY SHOP": //
			read = ("CITY SHOP : " + object.getCity_shop());
			break;
		case " HABITUAL CLIENT ": //
			read = ("HABITUAL CLIENT: " + object.getHabitual_client());
			break;
		case " DNI ":
			read = ("DNI : " + object.getdni());
			break;
		case "DATE OF BIRHT  ":
			read = ("DATE OF BIRHT : " + object.getdate_nac());
			break;
		case " ACCESSORIE ": //
			read = (" ACCESSORIE :" + object.getAccessories());
			break;
		case " DATE PURCHASE ": //
			read = ("DATE PURCHASE  : " + object.getdate_buy());
			break;
		case " DATE OF DELIVERY ":
			read = ("DELIVERY : " + object.delivery());
			break;
		case " PRICE ":
			read = ("PRICE :  " + object.getprice());
			break;
		case "All": // ALL
			read = object.toString();
			break;
		}

		return read;

	}

}
