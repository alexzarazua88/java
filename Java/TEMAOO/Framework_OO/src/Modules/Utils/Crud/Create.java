package Modules.Utils.Crud;


import Classes.dates;
import Modules.Classes.football;
import Modules.Classes.running;
import Modules.Classes.swimming;
import Modules.Classes.tennis;
import Modules.Utils.FunctionsDate;
import utils.ComboBox;
import utils.Numbers;
import utils.Validates_Data;

public class Create {
	
	
	
	//////////////////////////////////////  SWIMMING 
	
	public static swimming create_swimming() { // CREAR SWIMMING
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String size[] = { " XS", " S ", " M ", " L " };

		String user_type = ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users);
		String city_shop = Validates_Data.pidecity();
		String habitual_client = ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients);
		String dni = Validates_Data.pidedni();
		dates date_nac = FunctionsDate.date_nac("INTRO YOUR DATE OF BIRHT");
		String swimcuit_color = Validates_Data.swimciut_color();
		String swimcuit_size = ComboBox.comboBoxV(" Choose the size : ", " SIZE", size);
		dates date_buy = FunctionsDate.d_Today();
		dates dateAfter = FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY");

		return new swimming(user_type, city_shop, habitual_client,dni,date_nac, swimcuit_color, swimcuit_size, date_buy,dateAfter);
	}
	
	
	////////////////////////////////////////// FOOTBALL 
	

	public static football create_football() { // CREAR football
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
	    String brand_boats [] = { "NIKE " , " ADIDAS " , " PUMA "};
	    
	    
		String user_type = ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users);
		String city_shop = Validates_Data.pidecity();
		String habitual_client = ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients);
		String dni = Validates_Data.pidedni();
		dates date_nac = FunctionsDate.date_nac("INTRO YOUR DATE OF BIRHT");
		String football_type = Validates_Data.football_type();
		String brand = ComboBox.comboBoxV(" WHICH BRAND DO YOU WANT ? ", " BRAND ", brand_boats);
		String size = Validates_Data.size();
		int quantity = Numbers.inputNumber("Quantity", "Quantity");
		dates date_buy = FunctionsDate.d_Today();
		dates dateAfter = FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY");

		return new football(user_type, city_shop, habitual_client,dni,date_nac,football_type,brand,size, quantity, date_buy,dateAfter);
	}
	
	/////////////////////////////////////////////////// RUNNING 
	
	
	public static running create_running() { // CREAR RUNNING
		String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
		String clients[] = { " YES ", " NO " };
		String tshirt_options [] = { " long sleeve " , " short sleeve " , "THERMAL"};
	    String shorts_options [] =  { "long pants " , " short pants " };
	    
	    
		String user_type = ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users);
		String city_shop =Validates_Data.pidecity();
		String habitual_client = ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients);
		String dni = Validates_Data.pidedni();
		dates date_nac = FunctionsDate.date_nac("INTRO YOUR DATE OF BIRHT");
		String tshirt = ComboBox.comboBoxV("CHOOSE YOUR T-SHIRT TYPE ", " T-SHIRT", tshirt_options);
		String shorts = ComboBox.comboBoxV("CHOOSE YOUR SHORTS TYPE ", " SHORTS ", shorts_options);
		dates date_buy = FunctionsDate.d_Today();
		dates dateAfter = FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY");
		


		return new running(user_type, city_shop, habitual_client,dni,date_nac,tshirt,shorts, date_buy,dateAfter);
	}

	//////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////// TENNIS
	
		public static tennis create_tennis() { // CREAR TENNIS
			String users[] = { " MEN ", " WOMEN ", " CHILDREN " };
			String clients[] = { " YES ", " NO " };
			 String c_accessories [] = { "Tennis Racquets "  , 	" Wristbands " , " Tennis Balls" } ;
		    
		    
			String user_type = ComboBox.comboBoxV(" ARE YOU :", " USER_TIPE ", users);
			String city_shop = Validates_Data.pidecity();
			String habitual_client = ComboBox.comboBoxV(" ARE YOU AN HABITUAL CLIENT :", " HABITUAL CLIENT ", clients);
			String dni = Validates_Data.pidedni();
			dates date_nac = FunctionsDate.date_nac("INTRO YOUR DATE OF BIRHT");
			String accessories =  ComboBox.comboBoxV(" CHOOSE AN  ACCESSORIE :", " ACCESSORIE ", c_accessories);
			dates date_buy = FunctionsDate.d_Today();
			dates dateAfter = FunctionsDate.Date_AfterToday("INTRO THE DATE OF DELIVERY");


			return new tennis(user_type, city_shop, habitual_client,dni,date_nac,accessories, date_buy,dateAfter);
		}


}
