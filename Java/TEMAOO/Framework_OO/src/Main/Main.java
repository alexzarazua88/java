package Main;

import javax.swing.JOptionPane;

import Modules.Classes.football;
import Modules.Classes.running;
import Modules.Classes.swimming;
import Modules.Classes.tennis;
import Modules.Utils.Crud.Create;
import Modules.Utils.Crud.Functions_MainCrud;
import Modules.Utils.Crud.Read;
import Modules.Utils.Crud.Update;
import utils.ButtonBox;

public class Main {

	public static void main(String[] args) {
		swimming swimming1 = null;
		football football1 = null;
		running running1 = null;
		tennis tennis1 = null;

		int Options_crud = 0;
		int option = 0;
		boolean exit = false;
		String[] options = { "  SWIMMING   ", " FOOTBALL ", " RUNNING   ", "  TENNIS " , " SALIR " }; // +0, -1, *2 , /3

		do {
			option = ButtonBox.buttonsOptions(" MENU : ", " MAIN MENU ", options);
			switch (option) {

			case 0: // SWIMMING

				Options_crud = Functions_MainCrud.menu_crud("SWIMMING");

				if (Options_crud == 0) { // CREATE
					swimming1 = (swimming) Create.create_swimming();

				} else if (Options_crud == 1) { // READ
					if (swimming1 != null) {
						JOptionPane.showMessageDialog(null, Read.read_swimming(swimming1), "Read",
								JOptionPane.INFORMATION_MESSAGE);
					}

				} else if (Options_crud == 2) { // UPDATE
					if (swimming1 != null) {
						swimming1 = Update.update_swimming(swimming1);
					}
				} else if (Options_crud == 3) { // DELETE
					swimming1 = null;
					JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
				}

				break;

			case 1: // FOOTBALL

				Options_crud = Functions_MainCrud.menu_crud("FOOTBALL");

				if (Options_crud == 0) { // CREATE
					football1 = (football) Create.create_football();

				} else if (Options_crud == 1) { // READ
					if (football1 != null) {
						JOptionPane.showMessageDialog(null, Read.read_football(football1), "Read",
								JOptionPane.INFORMATION_MESSAGE);
					}

				} else if (Options_crud == 2) { // UPDATE
					if (football1 != null) {
						football1 = Update.update_football(football1);
					}
				} else if (Options_crud == 3) { // DELETE
					football1 = null;
					JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
				}

				break;

			case 2: // RUNNING

				Options_crud = Functions_MainCrud.menu_crud("RUNNING");

				if (Options_crud == 0) { // CREATE
					running1 = (running) Create.create_running();

				} else if (Options_crud == 1) { // READ
					if (running1 != null) {
						JOptionPane.showMessageDialog(null, Read.read_running(running1), "Read",
								JOptionPane.INFORMATION_MESSAGE);
					}

				} else if (Options_crud == 2) { // UPDATE
					if (running1 != null) {
						running1 = Update.update_running(running1);
					}
				} else if (Options_crud == 3) { // DELETE
					running1 = null;
					JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
				}

				break;

			case 3: // TENNIS

				Options_crud = Functions_MainCrud.menu_crud("TENNIS");

				if (Options_crud == 0) { // CREATE
					tennis1 = (tennis) Create.create_tennis();

				} else if (Options_crud == 1) { // READ
					if (tennis1 != null) {
						JOptionPane.showMessageDialog(null,Read.read_tennis(tennis1), "Read",
								JOptionPane.INFORMATION_MESSAGE);
					}

				} else if (Options_crud == 2) { // UPDATE
					if (tennis1 != null) {
						tennis1 = Update.update_tennis(tennis1);
					}
				} else if (Options_crud == 3) { // DELETE
					tennis1 = null;
					JOptionPane.showMessageDialog(null, "Deleted", "Deleted", JOptionPane.INFORMATION_MESSAGE);
				}

				break;

			default:
				exit = true;
				break;
			}

		} while (!exit);

	}// END_CLASS_MAIN

}