package Modules.Utils.Crud;

import javax.swing.JOptionPane;

import Modules.Classes.Singleton;
import Modules.Classes.football;
import Modules.Classes.running;
import Modules.Classes.swimming;
import Modules.Classes.tennis;
import Modules.Utils.Functions_Find;
import Modules.Utils.Functions_users;
import utils.ButtonBox;
//import utils.ComboBox;

public class Read {

	/////////////////////////////////////////////////////

	public static void read_swimming(swimming object) { // MOSTRAR SWIMMING
		System.out.println("entra en FUNCION read swimming ");

		String[] options = { "Read all", "Read One" };
		int size = Singleton.a_swimming.size();
		int position = -1;

		if (Singleton.a_swimming.isEmpty()) {
			System.out.println("SI EL objeto  ESTA vacio ");
			JOptionPane.showMessageDialog(null, "MAIN ERROR ", "ERROR", JOptionPane.ERROR_MESSAGE);
		} else {
			System.out.println("NO ESTA VACIO Y ENTRA PARA MOSTRAR EL READ");
			switch (ButtonBox.buttonsOptions("MENU READ", "MAIN MENU", options)) {
			case 0:
				System.out.println("Entra case all");
				for (int i = 0; i < size; i++) {
					String read = "";
					read = read + (Singleton.a_swimming.get(i).toString() + "\n\n");
					JOptionPane.showMessageDialog(null, read, "Swimming : "  + "\n\n", JOptionPane.INFORMATION_MESSAGE);
				}

				break;

			case 1:
				System.out.println("Entra case one");
				position = -1;
				object = Functions_users.resource_sCodref();
				System.out.println("COMPROVEM OBJECTE CASE ONE : " + object );
				position = Functions_Find.find_swimming(object);
				System.out.println("FIND position : " + position);
				if (position != -1) {
					System.out.println("ENTRA SI TROBA");
					object = Singleton.a_swimming.get(position);
					System.out.println( "CASE ONE : " + "  " + position);
					JOptionPane.showMessageDialog(null, object.toString() , " READ ONE " ,JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "usererror", "errortitle", JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
		}
		// return read;
	}

	/////////////////////////////////////////////////////

	public static void read_football(football football1) { // MOSTRAR FOTBALL
		
		String[] options = { "Read all", "Read One" };
		int size = Singleton.a_football.size();
		int position = -1;
		if (Singleton.a_football.isEmpty()) {
			JOptionPane.showMessageDialog(null, "MAIN ERROR ", "ERROR", JOptionPane.ERROR_MESSAGE);
		} else {
			System.out.println("NO ESTA VACIO Y ENTRA PARA MOSTRAR EL READ");
			switch (ButtonBox.buttonsOptions("MENU READ", "MAIN MENU", options)) {
			case 0:
				System.out.println("Entra case all football");
				for (int i = 0; i < size; i++) {
					String read = "";
					read = read + (Singleton.a_football.get(i).toString() + "\n\n" );
					JOptionPane.showMessageDialog(null, read, "FOOTBALL  : ", JOptionPane.INFORMATION_MESSAGE);
				}
				break;
			case 1:
				System.out.println("Entra case one");
				position = -1;
				football1 = Functions_users.resource_fCodref();
				System.out.println("COMPROVEM OBJECTE CASE ONE : " + football1 + "\n" + football1.getCod_Ref());
				position = Functions_Find.find_football(football1);
				System.out.println("FIND CSAE ONE: " + football1 + "\n" + football1.getCod_Ref());
				if (position != -1) {
					System.out.println("ENTRA SI TROBA");
					football1 = Singleton.a_football.get(position);
					System.out.println( "CASE ONE : " + football1 + "  "+ position);
					JOptionPane.showMessageDialog(null, football1.toString() , " READ ONE " ,JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "ERROR READ ONE ", "READ ONE ERROR", JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
		}
		//return read;

	}

	/////////////////////////////////////////////////////

	public static void read_running(running running1) { // MOSTRAR RUNNING
		System.out.println("entra en FUNCION read running ");
		
		String[] options = { "Read all", "Read One" };
		int position = -1;
		int size = Singleton.a_running.size();
		if (Singleton.a_running.isEmpty()) {
			JOptionPane.showMessageDialog(null, "MAIN ERROR ", "ERROR", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions("MENU READ", "MAIN MENU", options)) {
			case 0:
				System.out.println("Entra case all");
				for (int i = 0; i < size; i++) {
					String read = "";
					read = read + (Singleton.a_running.get(i).toString());
					JOptionPane.showMessageDialog(null, read, "running  : ", JOptionPane.INFORMATION_MESSAGE);
				}
				break;

			case 1:
				System.out.println("Entra case one");
				position = -1;
				running1 = Functions_users.resource_rCodref();
				System.out.println("COMPROVEM OBJECTE CASE ONE : " + running1 + "\n" + running1.getCod_Ref());
				position = Functions_Find.find_running(running1);
				System.out.println("FIND CSAE ONE: " + running1 + "\n" + running1.getCod_Ref());
				if (position != -1) {
					System.out.println("ENTRA SI TROBA");
					running1 = Singleton.a_running.get(position);
					System.out.println( "CASE ONE : " + running1 + "  "+ position);
					JOptionPane.showMessageDialog(null, running1.toString() , " READ ONE " ,JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "usererror", "errortitle", JOptionPane.ERROR_MESSAGE);
				}
				break;

			}
		}
		//return read;

	}

	/////////////////////////////////////////////////////

	public static void read_tennis(tennis tennis1) { // MOSTRAR TENNIS
		System.out.println("entra en FUNCION read running ");
	
		String[] options = { "Read all", "Read One" };
		int position = -1;
		int size = Singleton.a_tennis.size();
		if (Singleton.a_tennis.isEmpty()) {
			JOptionPane.showMessageDialog(null, "MAIN ERROR ", "ERROR", JOptionPane.ERROR_MESSAGE);
		} else {
			switch (ButtonBox.buttonsOptions("MENU READ", "MAIN MENU", options)) {
			case 0:
				System.out.println("Entra case all");
				for (int i = 0; i < size; i++) {
					String read = "";
					read = read + (Singleton.a_tennis.get(i).toString());
					JOptionPane.showMessageDialog(null, read, "running  : ", JOptionPane.INFORMATION_MESSAGE);
				}
				break;

			case 1:
				System.out.println("Entra case one");
				position = -1;
				tennis1 = Functions_users.resource_tCodref();
				System.out.println("COMPROVEM OBJECTE CASE ONE : " + tennis1 + "\n" + tennis1.getCod_Ref());
				position = Functions_Find.find_tennis(tennis1);
				System.out.println("FIND CSAE ONE: " + tennis1 + "\n" + tennis1.getCod_Ref());
				if (position != -1) {
					System.out.println("ENTRA SI TROBA");
					tennis1 = Singleton.a_tennis.get(position);
					System.out.println( "CASE ONE : " + tennis1 + "  "+ position);
					JOptionPane.showMessageDialog(null, tennis1.toString() , " READ ONE " ,JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "usererror", "errortitle", JOptionPane.ERROR_MESSAGE);
				}
				break;

			default:
				break;
			}
		}
		//return read;
	}

}
